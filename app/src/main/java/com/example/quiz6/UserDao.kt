package com.example.quiz6

import androidx.room.*


@Dao
    interface UserDao {
        @Query("SELECT * FROM user")
        fun getAll(): List<User>

        @Query("SELECT * FROM user WHERE uid IN (:userIds)")
        fun loadAllByIds(userIds: IntArray): List<User>

        @Query("SELECT * FROM user WHERE title LIKE :first AND " +
                "description LIKE :last LIMIT 1")
        fun findByName(first: String, last: String): User

        @Insert
        fun insertAll(vararg users: User)

        @Delete
        fun delete(user: User)

     @Query("UPDATE  User set title=:title , description=:desc WHERE uid ==:id ")
     fun updateTable(id:Int,title:String,desc:String)

    }
