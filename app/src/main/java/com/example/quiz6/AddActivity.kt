package com.example.quiz6

import android.app.Activity
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.room.Room
import kotlinx.android.synthetic.main.activity_add.*

class AddActivity : AppCompatActivity() {
    private val db by lazy {
        Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "database-name"
        ).build()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)
        init()
    }

    private fun init(){
        val title=gettitle.text
        val desc=getdesc.text
        val url=geturl.text
      //  val intent=intent
        savebtn.setOnClickListener(){
            if(title.length in 4..30 && desc.length in 32..300){
                AsyncTask.execute(){
                    db.userDao().insertAll(User(0,title.toString(),desc.toString(),url.toString()))
                    setResult(Activity.RESULT_OK,intent)
                    finish()
                }
            }
            else {
                Toast.makeText(this,"data was entered not correctly",Toast.LENGTH_SHORT).show()

            }
        }


    }

}
