package com.example.quiz6

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.items_layout.view.*
import kotlinx.coroutines.flow.callbackFlow


class Adapter(val mylist:MutableList<User>,val callback: Callback):RecyclerView.Adapter<Adapter.holder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        return holder(LayoutInflater.from(parent.context).inflate(R.layout.items_layout,parent,false))
    }

    override fun getItemCount(): Int {
        return mylist.size
    }

    override fun onBindViewHolder(holder: holder, position: Int) {
       holder.onbind()
    }

    inner class holder(itemView:View):RecyclerView.ViewHolder(itemView){
        private  lateinit var model:User

        fun onbind(){
            model=mylist[adapterPosition]
            itemView.title.text=model.title
            itemView.description.text=model.description
            Glide.with(itemView.context).load(Uri.parse(model.url)).into(itemView.image)
            itemView.mainitem.setOnClickListener(){
                callback.getclick(model,adapterPosition)
            }
            itemView.deletebtn.setOnClickListener(){
                callback.getdeleteclick(model)
            }

        }


    }
}