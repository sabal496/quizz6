package com.example.quiz6

import android.app.Activity
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.room.Room
import kotlinx.android.synthetic.main.activity_add.*
import kotlinx.android.synthetic.main.activity_edit.*

class editActivity : AppCompatActivity() {
    private val db by lazy {
        Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "database-name"
        ).build()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)
        init()
    }
    private fun init(){

        var model=intent.getParcelableExtra<MyModel>("user")
        edittitle.setText(model.title)
        editdesc.setText(model.description)
        editurl.setText(model.url)
        val inten=intent
        editsave.setOnClickListener(){
            val title=edittitle.text
            val desc=editdesc.text
            val url=editurl.text

            if(title.length in 4..30 && desc.length in 32..300){
                AsyncTask.execute(){
                    db.userDao().updateTable(model.id!!,title.toString(),desc.toString())
                }
                setResult(Activity.RESULT_OK,inten)
                finish()

            }
        }

    }
}
