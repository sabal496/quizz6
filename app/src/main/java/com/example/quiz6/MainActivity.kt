package com.example.quiz6

import android.app.Activity
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val db by lazy {
        Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "database-name"
        ).build()
    }
    var list= mutableListOf<User>()
    lateinit var adapter:Adapter
    var list2= mutableListOf<MyModel>()
    lateinit var model: MyModel

   companion object{
       val RESULT_CODE=1
       val EDIT_REQUEST_CODE=2
   }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        checkempty()

    }

    private fun init(){
     val model=MyModel("ragac satauri","ragac agwera","https://image.shutterstock.com/image-photo/bright-spring-view-cameo-island-260nw-1048185397.jpg")
        AsyncTask.execute(){
            db.userDao().insertAll(User(0,model.title,model.description,model.url))
        }
         AsyncTask.execute(){
           list=db.userDao().getAll().toMutableList()

             adapter= Adapter(list,object :Callback{
                 override fun getclick(user: User,posi:Int) {
                     var model:MyModel= MyModel(user.title,user.description,user.url,user.uid)


                      val intent=Intent(this@MainActivity,editActivity::class.java)
                     intent.putExtra("user",model)

                     startActivityForResult(intent, EDIT_REQUEST_CODE)
                 }

                 override fun getdeleteclick(user:User) {
                     list.remove(user)
                     AsyncTask.execute(){
                         db.userDao().delete(user)

                         runOnUiThread(Runnable {

                             adapter.notifyDataSetChanged()
                            // Toast.makeText(this@MainActivity,"ragaca",Toast.LENGTH_SHORT).show()
                         })
                     }
                     adapter.notifyDataSetChanged()
                 }
             })
             recycle.layoutManager=LinearLayoutManager(this)
             recycle.adapter=adapter
        }


        addbtn.setOnClickListener(){
            val intent=Intent(this,AddActivity::class.java)
            startActivityForResult(intent,RESULT_CODE)

        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode== RESULT_CODE && resultCode==Activity.RESULT_OK){
            AsyncTask.execute(){
                list=  db.userDao().getAll().toMutableList()
            }
            adapter.notifyDataSetChanged()
        }
        else if(requestCode== EDIT_REQUEST_CODE && resultCode==Activity.RESULT_OK){
            AsyncTask.execute(){
                list=  db.userDao().getAll().toMutableList()
            }




        }
        super.onActivityResult(requestCode, resultCode, data)
    }
    private fun   checkempty(){
        if(list.size<1)
            empty.visibility=View.GONE
    }
}
